var hotline = document.getElementById("txtHotline");
var province = document.getElementById("slProvince");
var _status = document.getElementById("slStatus");
var ordering = document.getElementById("txtOrdering");

var alertDanger = document.getElementById("alert-danger");

function validate() {
  if (hotline.value == "" || ordering.value == "") {
    setNotice(alertDanger, "Error, please fill all fields");
    return;
  } else if (province.value == "0" || _status.value == "0") {
    setNotice(alertDanger, "Error, please choose your option");
    return;
  } else if (!isPhoneNumberVietNam(hotline.value)) {
    setNotice(alertDanger, "Invalid hotline");
    return;
  } else {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Submit success", {
          icon: "success",
        });
      } else {
        swal("You just canceled");
      }
    });
  }
}

function isPhoneNumberVietNam(phonenumber) {
  var regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
  return regex.test(phonenumber);
}

function setNotice(input, message) {
  input.innerText = message;
  input.classList.remove("hide");
}
