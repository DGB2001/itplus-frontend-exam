var _name = document.getElementById("txtName");
var type = document.getElementById("txtType");
var time = document.getElementById("txtTime");
var _length = document.getElementById("txtLength");
var categoryschedule = document.getElementById("slCategorySchedule");
var _status = document.getElementById("slStatus");
var ordering = document.getElementById("txtOrdering");
var alertDanger = document.getElementById("alert-danger")

function validate() {
  if (
    _name.value == "" ||
    type.value == "" ||
    time.value == "" ||
    _length.value == "" ||
    ordering.value == ""
  ) {
    swal("Error", "Please fill all the fields", "error");
    return;
  } else if (categoryschedule.value == 0 || _status.value == 0) {
    swal("Error", "Please choose the options", "error");
    return;
  } else if(_length.value.length>8) {
      swal("Error", "Maximum number characters of Length is 8", "error");
    return;
  } else if(!isEmail(type.value)) {
    swal("Error", "Invalid email", "error");
    return;
  } else {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Submit success", {
          icon: "success",
        });
      } else {
        swal("You just canceled");
      }
    });
  }
}

function isEmail(email) {
  var regex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
  return regex.test(email);
}

function setNotice(input, message) {
    input.innerText = message;
    input.classList.remove('hide');
}