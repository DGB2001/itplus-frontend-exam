var btnSubmit = document.getElementById("btn-submit");

var idSchedule = document.getElementById("idSchedule");
var idPrice = document.getElementById("idPrice");
var idStartTime = document.getElementById("idStartTime");
var idOdering = document.getElementById("idOdering");
var alertDanger = document.getElementById("alert-danger");
//btnSubmit.addEventListener("click", checkBlank());

function validate() {
  checkBlank();
  priceGreaterThanZero();
  if (checkBlank() == true && priceGreaterThanZero() == true) {
    swal({
      title: "Are you sure?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("Submit success", {
          icon: "success",
        });
      } else {
        swal("You just canceled");
      }
    });
    return true;
  }
}

function checkBlank() {
  if (idSchedule.value == 0) {
    setNotice(alertDanger, "Bạn chưa chọn chuyến bay");
    return false;
  } else if (idPrice.value.trim() == "") {
    setNotice(alertDanger, "Bạn chưa nhập giá");
    return false;
  } else if (idStartTime.value.trim() == "") {
    setNotice(alertDanger, "Bạn chưa nhập thời gian bay");
    return false;
  } else if (idOdering.value.trim() == "") {
    setNotice(alertDanger, "Bạn chưa nhập order");
  } else {
    return true;
  }
}

function priceGreaterThanZero() {
  if (idPrice.value <= 0) {
    setNotice(alertDanger, "Giá phải lớn hơn 0");
    return false;
  } else {
    return true;
  }
}

function setNotice(input, message) {
  input.innerText = message;
  input.classList.remove("hide");
}
